const calculateDistance = (point1, point2) => {
    return Math.sqrt(
        Math.pow(point2.x - point1.x, 2) +
        Math.pow(point2.y - point1.y, 2))
}

const calculatePower = (distance, reach) => {
    return distance > reach
        ? 0
        : Math.pow(reach - distance, 2)
}

const findBestStation = (stationsData, point) => {
    const stations = stationsData.map(stationRow => {
        return {
            x: stationRow[0],
            y: stationRow[1],
            reach: stationRow[2]
        }
    }).map(station => {
        station.power = calculatePower(
            calculateDistance(point, station),
            station.reach);
        return station
    })
    result = stations.sort((a, b) => b.power - a.power)[0]

    return (result.power > 0)
        ? `Best link station for point ${point.x},${point.y} is ${result.x},${result.y} with power ${result.power}`
        : `No link station found for point ${point.x},${point.y}`
}

const stationsData = [
    [0, 0, 10],
    [20, 20, 5],
    [10, 0, 12]];

console.log(findBestStation(stationsData, { x: 1, y: 1 }))
console.log(findBestStation(stationsData, { x: 100, y: 100 }))
console.log(findBestStation(stationsData, { x: 15, y: 10 }))
console.log(findBestStation(stationsData, { x: 18, y: 18 }))